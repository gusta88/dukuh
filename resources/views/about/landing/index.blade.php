@extends('layouts.landing')
@section('content')
<section class="main-slider style2">
    <div class="slider-box">
        <!-- Banner Carousel -->
        <div class="banner-carousel owl-theme owl-carousel">
            <!-- Slide -->
            <div class="slide">
                <div class="image-layer" style="background-image:url({{asset('assets-guest/images/slides/slide-v3-1.jpg')}})"></div>
                <div class="auto-container">
                    <div class="content">
                        <h3>Kami Berada di</h3>
                        <h2>Dukuh Buahan</h2>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<section class="about-style1-area">
    <div class="container">
        <div class="row">

            <div class="col-xl-5 col-lg-6">
                <div class="about-style1-text-box">
                    <div class="sec-title">
                        <div class="title">
                            <h5>Tentang</h5>
                            <div class="border-box"></div>
                        </div>
                        <div class="big-title"><h2>Dukuh Buahan</h2></div>
                    </div>
                    <div class="inner-contant">
                        {!!$item->isi!!}
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
@endsection
