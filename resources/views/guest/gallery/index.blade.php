@extends('layouts.landing')
@section('content')
<section id="blog-area" class="blog-style1-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="fotorama" data-nav="thumbs"  data-allowfullscreen="true">
                    @foreach ($data as $item)
                    <a href="$item->directory)g"><img src="{{asset($item->directory)}}" width="144" height="96"></a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('css')
<link rel="stylesheet" href="{{asset('pages/fotorama/fotorama.css')}}">
@endpush

@push('scripts')
<!-- Core JS file -->
<script src="{{asset('pages/fotorama/fotorama.js')}}"></script>
@endpush

@push('docred')

@endpush
