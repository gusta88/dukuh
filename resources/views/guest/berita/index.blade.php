@extends('layouts.landing')
@section('content')
<!-- Start Main Slider -->
<section class="main-slider style2">
    <div class="slider-box">
        <!-- Banner Carousel -->
        <div class="banner-carousel owl-theme owl-carousel">
            <!-- Slide -->
            <div class="slide">
                <div class="image-layer" style="background-image:url({{asset('assets-guest/images/slides/background.jpg')}})"></div>
                <div class="auto-container">
                    <div class="content">
                        <h3></h3>
                        <h2>Berita Dukuh Buahan</h2>

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- End Main Slider -->
<section id="blog-area" class="blog-style1-area">
    <div class="container">
        <div class="row">
            <div class="col-1"></div>
            <div class="col-lg-11 col-xs-12 align-items-center">
                <div class="row justify-center">
                    @foreach ($data as $item)
 <!--Start single blog post-->
                    <div class=" col-lg col-xs mr-2 border single-blog-post ">
                        <div class="img-holder">
                            <div class="inner">
                                <a href="{{route('singleberita',$item->posting_id)}}">
                            <img src="{{asset($item->thumbnail)}}" @if ($item->thumbnail = ' http://dukuh:8888/upload/posting/thumbnail/default.jpg')  @endif alt="Awesome Image">
                                </a>
                            </div>
                        </div>
                        <div class="text-holder">
                            <ul class="meta-info">
                            <li><i class="fa fa-calendar thm-clr1" aria-hidden="true"></i><a href="#">{{\Carbon\Carbon::parse($item->created_at)->format('d F Y')}}</a></li>

                            </ul>
                        <h3 class="blog-title"><a href="{{route('singleberita',$item->posting_id)}}">{{$item->judul}}</a></h3>

                        </div>
                    </div>
                    <!--End single blog post-->
                    @endforeach

        </div>
        {{$data->links('vendor.pagination.bootstrap-4')}}
    </div>

    <div class="col-1"></div>
        </div>
    </div>
</section>
@endsection
