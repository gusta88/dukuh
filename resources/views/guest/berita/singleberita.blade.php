@extends('layouts.landing')
@section('content')
<!-- Start Main Slider -->
<section class="main-slider style2">
    <div class="slider-box">
        <!-- Banner Carousel -->
        <div class="banner-carousel owl-theme owl-carousel">
            <!-- Slide -->
            <div class="slide">
                <div class="image-layer" style="background-image:url({{asset($item->thumbnail)}})"></div>
                <div class="auto-container">
                    <div class="content">
                        <h3></h3>
                        <h2>{{$item->judul}}</h2>

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- End Main Slider -->
<section id="blog-area" class="blog-single-area">
    <div class="container">
        <div class="row">
            <div class="col-1"></div>
            <div class="col-lg-10 col-xs-12 align-items-center">
                <div class="blog-post">
                    <!--Start single blog post-->
                    <div class="single-blog-post">
                        <div class="img-holder">
                            <div class="inner">

                            </div>

                        </div>
                       <div class="text-holder">
                        <ul class="meta-info">
                        <li><i class="fa fa-calendar thm-clr1" aria-hidden="true"></i><a href="#">{{\Carbon\Carbon::parse($item->created_at)->format('d F Y')}}</a></li>

                            <li><i class="fa fa-user thm-clr1" aria-hidden="true"></i><a href="#">Admin</a></li>
                        </ul>
                       <h3 class="blog-title">{{$item->judul}}</h3>
                            {!!$item->isi!!}
                       </div>
                    </div>
                    <!--End single blog post-->
                </div>
            </div>

        </div>
    </div>
</section>
@endsection
