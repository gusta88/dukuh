@extends('layouts.landing')
@section('content')


<!-- Start Main Slider -->
<section class="main-slider style2">
    <div class="slider-box">
        <!-- Banner Carousel -->
        <div class="banner-carousel owl-theme owl-carousel">
            <!-- Slide -->
            <div class="slide">
                <div class="image-layer" style="background-image:url({{asset('assets-guest/images/slides/background.jpg')}})"></div>
                <div class="auto-container">
                    <div class="content">
                        <h3>Desa Adat</h3>
                        <h2>Dukuh Buahan</h2>

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- End Main Slider -->

<!--Start About Style3 Area-->
<section class="about-style3-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-9 col-md-10">
                <div class="about-style3-image-box">
                    <div class="patter-box" style="background-image:url(assets/images/pattern/pattern-bg-4.png)"></div>
                    <div class="inner">
                        <img src="assets/images/about/about-3.jpg" alt="Awesome Image">
                    </div>

                </div>
            </div>
            <div class="col-xl-6 col-lg-12">
                <div class="about-style3-text-box">
                    <div class="sec-title-style4">
                        <p>TENTANG DUKUH BUAHAN</p>
                        <div class="big-title"><h2>DUKUH BUAHAN<br> DENBANTAS, TABANAN, BALI</h2></div>
                        <div class="border-box">
                            <div class="borders-left thm-bgc3"></div>
                            <div class="borders-right thm-bgc3"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!--End About Style3 Area-->

<!--Start Services Style5 Area-->
<section class="services-style5-area">
    <div class="pattern-bg" style="background-image:url(assets/images/pattern/pattern-bg-7.png);"></div>
    <div class="container">
        <div class="sec-title-style4 clr-white text-center wow slideInUp animated" data-wow-delay="0.3s" data-wow-duration="1200ms">
            <p>SEKILAS</p>
            <div class="big-title"><h2>Galer dari <br> DUKUH BUAHAN</h2></div>
            <div class="border-box center">
                <div class="borders-left thm-bgc3"></div>
                <div class="borders-right thm-bgc3"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="theme-carousel services-carousel-style5 owl-carousel owl-theme owl-dot-style1 thm-border-clr4" data-options='{"loop":true, "margin":30, "autoheight":true, "nav":false, "dots":true, "autoplay":true, "autoplayTimeout":10000, "smartSpeed":700, "responsive":{ "0":{"items": "1"}, "768":{"items": "2"}, "1199":{"items": "3" }}}'>
                    <!--Start Single service Style5-->
                    @foreach ($gambar as $img)
                    <div class="single-service-style5 wow animated fadeInUp" data-wow-delay="0.1s">
                        <div class="img-holder">

                        <img src="{{asset($img->directory)}}" alt="Awesome Image">
                            <div class="overlay-style-one bg4">
                                <div class="box">
                                    <div class="content">
                                        <div class="link-button">
                                            <a href="#"><span class="flaticon-link"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Start Single service Style5-->
                    @endforeach


                </div>

            </div>
        </div>
    </div>
</section>
<!--End Services Style5 Area-->


<!--Start Work Process Area Style2-->
<section class="latest-blog-style4-area">
    <div class="container">
        <div class="sec-title-style4 text-center">
            <p>Dukuh Buahan</p>
            <div class="big-title"><h2>Berita Terakhir</h2></div>
            <div class="border-box center">
                <div class="borders-left thm-bgc3"></div>
                <div class="borders-right thm-bgc3"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="latest-blog-style4-outer">
                    <div class="blog-style4-carousel owl-carousel owl-theme">
                        <!--Start single blog post style4-->
                        @foreach ($posting as $post)
                        <div class="single-blog-post-style4">
                            <div class="img-holder">
                                <div class="inner">
                                <img src="{{asset($post->thumbnail)}}" alt="Awesome Image">
                                </div>
                                <div class="title-holder">
                                    <ul class="meta-info">
                                        <li><i class="fa fa-calendar thm-clr4" aria-hidden="true"></i><a href="#">{{\Carbon\Carbon::parse($post->created_at)->format('d F Y')}}</a></li>
                                    </ul>
                                <h3 class="blog-title"><a href="#">{{$post->judul}}</a></h3>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        <!--End single blog post style4-->

                    </div>

                    <div class="btn-block">
                        <button class="prev-btn"></button>
                        <button class="next-btn"></button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!--End Work Process Area Style2-->
<!--google map> -->
<section class="m-10">

    <div class="container p-10 m-10">
        <div class="sec-title-style4 text-center">
            <p>Dukuh Buahan</p>
            <div class="big-title"><h2>Lokasi Desa</h2></div>
            <div class="border-box center">
                <div class="borders-left thm-bgc3"></div>
                <div class="borders-right thm-bgc3"></div>
            </div>
        </div>
        <div style="width: 100%"><iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=pura%20dalem%20desa%20pekraman%20dukuh%20buahan+()&amp;t=&amp;z=17&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe><a href="https://www.maps.ie/route-planner.htm">Google Route Planner</a></div>
    </div>
</section>
<!-- end google map> -->
<!--Start Partner Area-->
<section class="partner-area thm-bgc4">
    <div class="layer-outer" data-aos="fade-right" data-aos-easing="linear" data-aos-duration="700"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="partner-box">
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="{{asset('assets-guest/images/logo/tabanan.png')}}" width="100" height="auto" alt="Awesome Image"></a>
                    </div>
                    <!--End Single Partner Logo Box-->

                </div>
            </div>
        </div>
    </div>
</section>
<!--End Partner Area-->

<!--Start footer area-->

<!--End footer area-->
@endsection
