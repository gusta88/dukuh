<!DOCTYPE html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Dukuh Buahan') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('asset-guest/sass/app.css?v=00.00006')}}">
        <link rel="stylesheet" href="{{asset('assets-guest/css/flaticon.css')}}">
        <link rel="stylesheet" href="{{asset('assets-guest/css/font-awesome.min.css')}}">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets-guest/images/favicon/apple-touch-icon.png')}}">
        <link rel="icon" type="image/png" href="{{asset('assets-guest/images/favicon/favicon-32x32.png')}}" sizes="32x32">
        <link rel="icon" type="image/png" href="{{asset('assets/images/favicon/favicon-16x16.png')}}" sizes="16x16">
        <style>
            .page-item.active .page-link{
                background-color:#fb7162 !important;
                border-color:#fb7062 !important;
                color:#fff !important;
            }
            .page-link{
                color:#fb7062 !important;
            }
            .header-title{
                color: #fff;
                font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
                font-size: 20px;
                padding: 17px 0 0 0;
            }
        </style>
        @stack('css')
        <!-- Scripts -->
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.3/dist/alpine.js" defer></script>
    </head>
    <body>
        <div class="boxed_wrapper">
            <header class="main-header header-style-four">
                <!--Start header lawer -->
                <div class="row">
                <div class="container">

                        <div class="container-fluid">
                        <div class="col-3 m-0 pull-left">
                            <!--<img src="#" alt="font awesome" class="img">-->
                            <h1 class="header-title">Dukuh Buahan</h1>
                        </div>
                        <div class="col-9 pull-right">


                            <!--Top Left-->
                            <div class="header-lawer-left-style4 float-right">
                                <div class="nav-outer clearfix">
                                    <!--Mobile Navigation Toggler-->
                                    <div class="mobile-nav-toggler">
                                        <div class="inner">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </div>
                                    </div>
                                    <!-- Main Menu -->
                                    <nav class="main-menu style4 navbar-expand-md navbar-light">
                                        <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                            <ul class="navigation clearfix">
                                               <!-- <li class="current">
                                                    <a class="home-icon" href="#">Beranda</a>
                                                </li>-->
                                            <li class="current"><a href="{{route('berita')}}">Berita</a></li>
                                               <!-- <li><a href="#">Galery</a></li>
                                                <li><a href="#">Tentang Dukuh</a></li>-->
                                            </ul>
                                        </div>
                                    </nav>
                                    <!-- Main Menu End-->
                                </div>
                            </div>

                            </div>
                        </div>

                        </div>

                </div>
                <!--End header lawer -->

                <!--Sticky Header-->
                <div class="sticky-header white-bg">

                </div>
                <!--End Sticky Header-->



                <!-- Mobile Menu  -->
                <div class="mobile-menu">
                    <div class="menu-backdrop"></div>
                    <div class="close-btn"><span class="icon flaticon-multiply"></span></div>

                    <nav class="menu-box">
                        <div class="nav-logo"><a href="index.html"><img src="assets/images/resources/logo-7.png" alt="" title=""></a></div>
                        <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
                    </nav>
                </div>
                <!-- End Mobile Menu -->

            </header>
            @yield('content')
            </div>
        </div>
        <footer class="footer-area style4">
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <!--Start single footer widget-->
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 wow animated fadeInUp" data-wow-delay="0.3s">
                            <div class="single-footer-widget marbtm50">
                                <div class="our-company-info">
                                    <div class="footer-logo">
                                        <h2>Dukuh Buahan</h2>
                                    </div>
                                    <div class="text">
                                        <p>Website Dukuh Buahan</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End single footer widget-->

                    </div>
                </div>
            </div>
        </footer>
    </body>
    <script src="{{asset('assets-guest/js/jquery.js')}}"></script>
    <script src="{{asset('assets-guest/js/aos.js')}}"></script>
    <script src="{{asset('assets-guest/js/appear.js')}}"></script>
    <script src="{{asset('assets-guest/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets-guest/js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('assets-guest/js/isotope.js')}}"></script>
    <script src="{{asset('assets-guest/js/jquery.bootstrap-touchspin.js')}}"></script>
    <script src="{{asset('assets-guest/js/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('assets-guest/js/jquery.countTo.js')}}"></script>
    <script src="{{asset('assets-guest/js/jquery.easing.min.js')}}"></script>
    <script src="{{asset('assets-guest/js/jquery.enllax.min.js')}}"></script>
    <script src="{{asset('assets-guest/js/jquery.fancybox.js')}}"></script>
    <script src="{{asset('assets-guest/js/jquery.mixitup.min.js')}}"></script>
    <script src="{{asset('assets-guest/js/jquery.paroller.min.js')}}"></script>
    <script src="{{asset('assets-guest/js/jquery.polyglot.language.switcher.js')}}"></script>
    <script src="{{asset('assets-guest/js/map-script.js')}}"></script>
    <script src="{{asset('assets-guest/js/nouislider.js')}}"></script>
    <script src="{{asset('assets-guest/js/owl.js')}}"></script>
    <script src="{{asset('assets-guest/js/validation.js')}}"></script>
    <script src="{{asset('assets-guest/js/wow.js')}}"></script>
    <script src="{{asset('assets-guest/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('assets-guest/js/slick.js')}}"></script>
    <script src="{{asset('assets-guest/js/lazyload.js')}}"></script>
    <script src="{{asset('assets-guest/js/scrollbar.js')}}"></script>

    <!-- thm custom script -->
    <script src="{{asset('assets-guest/js/custom.js')}}"></script>
    @stack('scripts')
    <script>
        $( document ).ready(function() {
            @stack('docred')
        });
    </script>

</html>
