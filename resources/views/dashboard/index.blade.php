@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">

                </div>
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Posting Berita</h4>
                <form method="POST" enctype="multipart/form-data" action="{{route('posting.store')}}">
                    @csrf
                    <div class="row form-group @if ($errors->has('judul')) has-error @endif">
                        <div class="col-2">
                            Judul Posting
                        </div>
                        <div class="col-10">
                        <input type="text"  class="form-control" name="judul" placeholder="Judul">
                        @if ($errors->has('judul'))
                                <span class="help-block">
                                     <strong style="color:red;" class="red">{{ $errors->first('judul') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <!-- basic summernote-->
                    <div class="row form-group @if ($errors->has('isi')) has-error @endif">
                        <div class="col-2">
                            Isi
                        </div>
                        <div class="col-10">
                            <textarea id="summernote-basic" name="isi"></textarea>
                            @if ($errors->has('isi'))
                                <span class="help-block">
                                     <strong style="color:red;" class="red">{{ $errors->first('isi') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group @if ($errors->has('thumbnail')) has-error @endif">
                        <div class="col-2">
                            Thumbnail
                        </div>
                        <div class="col-10">
                            <input class="form-control-file" type="file" name="thumbnail">
                            <span> Optional </span>
                            @if ($errors->has('thumbnail'))
                                <span class="help-block">
                                     <strong style="color:red;" class="red">{{ $errors->first('thumbnail') }}</strong>
                                </span>
                            @endif
                        </div>

                    </div>
                    <div class="row form-group @if ($errors->has('doc')) has-error @endif">
                        <div class="col-2">
                            Document
                        </div>
                        <div class="col-10">
                            <input class="form-control-file" type="file" name="doc">
                            <span>Optional</span>
                            @if ($errors->has('doc'))
                                <span class="help-block">
                                     <strong style="color:red;" class="red">{{ $errors->first('doc') }}</strong>
                                </span>
                            @endif
                        </div>

                     </div>
                    <div class="form-group">
                        <button class="btn btn-blue">Simpan</button>
                    </div>
                </form>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div><!-- end col -->
    </div>
@endsection
@push('css')
        <link href="{{asset('assets/libs/summernote/summernote-bs4.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
        <script src="{{asset('assets/libs/summernote/summernote-bs4.min.js')}}"></script>

        <!-- Init js -->
        <script src="{{asset('assets/js/pages/form-summernote.init.js')}}"></script>
        <script src="{{asset('assets/js/pages/form-validation.init.js')}}"></script>
@endpush
@push('docred')
    $(document).ready(function() {
    $(".parsley-examples").parsley()
        }), $(function() {
    $("#demo-form").parsley().on("field:validated", function() {
        var e = 0 === $(".parsley-error").length;
        $(".alert-info").toggleClass("d-none", !e), $(".alert-warning").toggleClass("d-none", e)
    }).on("form:submit", function() {
        return !1
    })
    });
@endpush
