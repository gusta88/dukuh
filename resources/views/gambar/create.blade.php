@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="page-title-box">
        <div class="page-title-right">

        </div>
        <h4 class="page-title">Gambar</h4>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Gambar</h4>
                <form action="{{route('gambar.store')}}" method="post" enctype="multipart/form-data">
                    @csrf


                    <div class="fallback form-group @if ($errors->has('gambar')) has-error @endif">
                        <input name="gambar" class="form-control-file" type="file" />
                        @if ($errors->has('gambar'))
                                <span class="help-block">
                                     <strong style="color:red;" class="red">{{ $errors->first('gambar') }}</strong>
                                </span>
                            @endif
                    </div>




                <!-- Preview -->
                <div class="dropzone-previews mt-3" id="file-previews"></div>
                <div class="form-group">
                    <button class="btn btn-pink">Simpan</button>
                </div>

            </form>

            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div><!-- end col -->
</div>
@endsection
@push('css')
<link href="{{asset('assets/libs/dropzone/min/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
<script src="{{asset('assets/libs/dropzone/min/dropzone.min.js')}}"></script>
<script src="{{asset('assets/libs/dropify/js/dropify.min.js')}}"></script>
<!--<script src="{{asset('assets/js/pages/form-fileuploads.init.js')}}"></script>-->
@endpush
@push('docred')
Dropzone.options.dropzoneForm = {
    autoProcessQueue : false,
    acceptedFiles : ".png,.jpg,.gif,.bmp,.jpeg",

    init:function(){
      var submitButton = document.querySelector("#submit-all");
      myDropzone = this;

      submitButton.addEventListener('click', function(){
        myDropzone.processQueue();
      });

      this.on("complete", function(){
        if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
        {
          var _this = this;
          _this.removeAllFiles();
        }
        load_images();
      });

    }

  };

  load_images();

  function load_images()
  {
    $.ajax({
      url:"{{ route('gambar.store') }}",
      success:function(data)
      {
        $('#uploaded_image').html(data);
      }
    })
  }

 

@endpush
