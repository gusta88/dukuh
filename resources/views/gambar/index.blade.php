@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">

                </div>
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Gambar</h4>
                <div class="form-group"><a href="{{route('gambar.create')}}" class="btn btn-info">Tambah Gambar</a>
                    <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>Status</th>
                                <th>Gambar</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                         <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <td></td>
                                <td><img src="{{asset($item->directory)}}" width="144" height="96"></td>
                            <td><form class="pull-left" action="{{route('gambar.destroy',$item->gambar_id)}}" method="POST">@csrf <input type="hidden" name="_method" value="delete" /><button type="submit"   class="btn btn-danger" >Delete</button></td></form>
                            </tr>
                        @endforeach
                     </tbody>
                    </table>

    </div>
@endsection

@push('css')
    <link href="{{asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css?v=0.00000001')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css?v=0.00000001')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css?v=0.00000001')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/datatables.net-select-bs4/css/select.bootstrap4.min.css?v=0.00000001')}}" rel="stylesheet" type="text/css" />

@endpush
@push('scripts')
<!-- third party js -->
        <script src="{{asset('assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
        <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
        <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
        <script src="{{asset('assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
        <script src="{{asset('assets/libs/datatables.net-select/js/dataTables.select.min.js')}}"></script>
        <script src="{{asset('assets/libs/pdfmake/build/pdfmake.min.js')}}"></script>
        <script src="{{asset('assets/libs/pdfmake/build/vfs_fonts.js')}}"></script>

        <!-- third party js ends -->
   <!--<script src="{{asset('assets-guest/js/pages/datatables.init.js')}}"></script>-->
   <script src="{{asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
   <script src="{{asset('assets/js/pages/sweet-alerts.init.js')}}"></script>

        <!-- Sweet alert init js-->



@endpush

@push('docred')
   $("#basic-datatable").DataTable({
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>"
            }
        },
        drawCallback: function() {
            $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
        }
    });
$(".warning").click(function() {
            var id = $(this).data("value");
            var token = $(this).data("token");
            $.ajax(
            {
            url: " /gambar/"+id,
                type: 'PUT',
                dataType: "JSON",
                data: {
                    "id": id,
                    "_method": 'DELETE',
                    "_token": {{ csrf_token() }},
                },
                success: function ()
                {
                    console.log("it Work");
                },
                error: function()
                {
                    console.log("it doesn't Work");
                }
            });

            console.log("It failed");
        });

@endpush
