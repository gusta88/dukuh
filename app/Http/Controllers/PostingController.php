<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\posting;
use App\Models\gambar;
use App\Models\files;
use Intervention\Image\Facades\Image;

class PostingController extends Controller
{
    public function store(Request $request)
    {

        \Validator::make($request->all(),[
            'judul' => 'required',
            'isi' => 'required',
            'image' => 'nullable',
            'thumbnail' => 'mimes:jpeg,png',
            'doc' => 'mimetypes:application/pdf|max:10000'
        ])->setAttributeNames([
            'judul' => 'Judul',
            'isi' => 'Isi',
            'image' => 'Gambar',
            'thumbnail' => 'Thumbnail',
            'doc' => 'DOC'
        ])->validate();

        $item = new posting;
        $item->judul = $request->judul;
        $description=$request->isi;
        $dom = new \DomDocument();
        $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
            //upload image jika ada image di summeronte

        foreach($images as $key => $img){
            $data = $img->getAttribute('src');

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);


            $directory = "/upload/posting/images/";
            $image_name= time().$key.'.png';


            $path = public_path() . $directory . $image_name;
           file_put_contents($path, $data);

            $img->removeAttribute('src');
            $img->setAttribute('src', $directory.$image_name);
        }
        $description = $dom->saveHTML();
        $item->isi = $description;
        $item->landing = '1';
        if($request->hasFile('thumbnail')){
            if ($request->file('thumbnail')->isValid()) {
                //
                $extension = $request->thumbnail->extension();
                $thumbnail = time().".".$extension;

                $img = Image::make($request->thumbnail);
                $img->resize(770,null,function($cons){
                    $cons->aspectRatio();
                })->save(public_path() . '/upload/posting/thumbnail/' . $thumbnail);

               // $request->file('thumbnail')->storeAs('/upload/posting/thumbnail/', time().".".$extension);
               //$thumbnail = time().".".$extension;
                $item->thumbnail = '/upload/posting/thumbnail/'. $thumbnail;
                //$path = public_path() . '/upload/posting/thumbnail/' . $thumbnail;
                //$request->thumbnail->move(public_path('/upload/posting/thumbnail/'), $thumbnail);
              //file_put_contents($path, $request->file('thumbnail'));
            }
        }else{
            $item->thumbnail = '/upload/posting/thumbnail/def.jpg';
        }

        $item->status = '1';
        $item->category = 'posting';
        $item->save();
        if($request->hasFile('doc')){
            if ($request->file('doc')->isValid()) {
                $extension = $request->doc->extension();
                $request->file('doc')->storeAs('/upload/posting/doc/', time().".".$extension);
                $lembar =  new files();
                $lembar->nama = time().".".$extension;
                $lembar->directory = 'upload/posting/doc/'. $lembar->nama;
                $lembar->category = "posting";
                $lembar->landing = '0';
                $path = public_path() . '/upload/posting/doc/' . $lembar->nama;
                $request->doc->move(public_path('/upload/posting/doc/'), $lembar->nama);
              $lembar->id_posting = $item->posting_id;
              $lembar->save();
            }
        }else{
            $item->thumbnail = '1';
        }
        session()->flash('success', 'Posting Ditambahkan');
        return redirect()->back();
    }

    public function index()
    {
       $data = posting::orderBy('posting_id','DESC')->where('category','!=','tentang')->get();
       return view('posting.index')
       ->with('data',$data);
    }

    public function tentang()
    {
       $item = posting::where('category','tentang')->first();
       return view('about.index')
       ->with('item',$item);
    }

    public function addtentang(Request $request)
    {
        \Validator::make($request->all(),[
            'judul' => 'required',
            'isi' => 'required',
            'image' => 'nullable',
            'thumbnail' => 'mimes:jpeg,png',
            'doc' => 'mimetypes:application/pdf|max:10000'
        ])->setAttributeNames([
            'judul' => 'Judul',
            'isi' => 'Isi',
            'image' => 'Gambar',
            'thumbnail' => 'Thumbnail',
            'doc' => 'DOC'
        ])->validate();
        $item = posting::where('category','tentang')->first();

        $item->judul = $request->judul;
        $description=$request->isi;
        $dom = new \DomDocument();
        $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');

        foreach($images as $key => $img){
            $data = $img->getAttribute('src');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            $directory = "/upload/posting/images/";
            $image_name= time().$key.'.png';
            $path = public_path() . $directory . $image_name;
            file_put_contents($path, $data);

            $img->removeAttribute('src');
            $img->setAttribute('src', $directory.$image_name);
        }

        $description = $dom->saveHTML();
        $item->isi = $description;
        $item->landing = '1';
        if($request->hasFile('thumbnail')){
            if ($request->file('thumbnail')->isValid()) {
                //
                $extension = $request->thumbnail->extension();
               // $request->file('thumbnail')->storeAs('/upload/posting/thumbnail/', time().".".$extension);
               $thumbnail = time().".".$extension;
                $item->thumbnail = '/upload/posting/thumbnail/'. time().".".$extension;
                $path = public_path() . '/upload/posting/thumbnail/' . $thumbnail;
                $request->thumbnail->move(public_path('/upload/posting/thumbnail/'), $thumbnail);
              //file_put_contents($path, $request->file('thumbnail'));
            }
        }else{
            $item->thumbnail = '/upload/posting/thumbnail/default.jpg';
        }

        $item->status = '1';
        $item->categotry = 'tentang';
        $item->save();
    }

    public function destroy($id,Request $request)
    {
        $item = posting::find($id);
        if($item->thumbnail != '/upload/posting/thumbnail/def.jpg'){
            \File::delete(public_path($item->thumbnail));
        }

        $item->delete();
        return response()->json(['success']);
    }


    public function edit($id)
    {
        $item = posting::find($id);
        return view('posting.edit')
        ->with('item',$item)
        ;
    }
    public function update($id,Request $request)
    {
        \Validator::make($request->all(),[
            'judul' => 'required',
            'isi' => 'required ',
            'image' => 'nullable',
            'thumbnail' => 'mimes:jpeg,png',
            'doc' => 'mimetypes:application/pdf|max:10000'
        ])->setAttributeNames([
            'judul' => 'Judul',
            'isi' => 'Isi',
            'image' => 'Gambar',
            'thumbnail' => 'Thumbnail',
            'doc' => 'DOC'
        ])->validate();

        $item = posting::find($id);
        $item->judul = $request->judul;
        $description=$request->isi;
        $dom = new \DomDocument();
        $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');

        foreach($images as $key => $img){
            $data = $img->getAttribute('src');

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            $directory = "/upload/posting/images/";
            $image_name= time().$key.'.png';
            $path = public_path() . $directory . $image_name;
           file_put_contents($path, $data);

            $img->removeAttribute('src');
            $img->setAttribute('src', $directory.$image_name);
        }
        $description = $dom->saveHTML();
        $item->isi = $description;
        $item->landing = '1';
        if($request->hasFile('thumbnail')){
            if ($request->file('thumbnail')->isValid()) {
                //penentuan hapus thumbnail
                if($item->thumbnail != '/upload/posting/thumbnail/default.jpg'){
                    \File::delete(public_path($item->thumbnail));
                }

                //
                $extension = $request->thumbnail->extension();
               // $request->file('thumbnail')->storeAs('/upload/posting/thumbnail/', time().".".$extension);
               $thumbnail = time().".".$extension;
                $item->thumbnail = '/upload/posting/thumbnail/'. time().".".$extension;
                $path = public_path() . '/upload/posting/thumbnail/' . $thumbnail;
                $request->thumbnail->move(public_path('/upload/posting/thumbnail/'), $thumbnail);
              //file_put_contents($path, $request->file('thumbnail'));
            }
        }else{
            $item->thumbnail = '/upload/posting/thumbnail/default.jpg';
        }

        $item->status = '1';
        $item->category = 'posting';
        $item->save();
        if($request->hasFile('doc')){
            if ($request->file('doc')->isValid()) {
                //penentuan hapus doc
                if($item->file){
                    \File::delete(public_path($item->file->directory));
                    $delete_file = files::find($item->file->file_id);
                    $delete_file->delete();
                }
                $extension = $request->doc->extension();
                $request->file('doc')->storeAs('/upload/posting/doc/', time().".".$extension);
                $lembar =  new files();
                $lembar->nama = time().".".$extension;
                $lembar->directory = 'upload/posting/doc/'. $lembar->nama;
                $lembar->category = "posting";
                $lembar->landing = '0';
                $path = public_path() . '/upload/posting/doc/' . $lembar->nama;
                $request->doc->move(public_path('/upload/posting/doc/'), $lembar->nama);
              $lembar->id_posting = $item->posting_id;
              $lembar->save();
            }
        }else{
            $item->thumbnail = '1';
        }
        session()->flash('success', 'Posting Diedit');
        return redirect()->back();
    }
}
