<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\posting;
use App\Models\gambar;
use Image;

class GambarController extends Controller
{
    public function index()
    {
        //$data = gambar::groupBy('category')->select('category',\DB::raw('count(*) as total'))->get();
        $data = gambar::orderBy('created_at','DESC')->get();

        return view('gambar.index')
        ->with('data',$data)
        ;
    }

    public function create()
    {

        return view('gambar.create');
    }
    public function store(Request $request)
    {
        \Validator::make($request->all(),[
            'gambar' => 'required | mimes : jpg,png,jpeg | max : 20000',

        ])->setAttributeNames([
            'gambar' => 'Gambar',

        ])->validate();



                $item = new gambar();
                //
                $extension = $request->gambar->extension();
               // $request->file('thumbnail')->storeAs('/upload/posting/thumbnail/', time().".".$extension);
               $thumbnail = time().".".$extension;

                $path = '/upload/posting/images/';
                $item->directory = '/upload/posting/images/'. $thumbnail;

                $request->gambar->move(public_path($path),$thumbnail);

                $item->nama = $thumbnail;
                $item->category = 'galery';
                $item->landing = 1;
                $item->save();
              //file_put_contents($path, $request->file('thumbnail'));


        session()->flash('success', 'Gambar Ditambahkan');
        return redirect()->back();
    }

    public function destroy($id,Request $request)
    {

        $item = gambar::find($id);
        $item->delete();
        \File::delete(public_path($item->directory));
        session()->flash('success', 'Gambar Dihapus');
        return redirect()->back();

    }

}
