<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\posting;
use App\Models\gambar;
use Illuminate\Support\Facades\DB;

class GuestController extends Controller
{
    public function index()
    {
        $posting = posting::orderBy('created_at','DESC')->take(5)->get();
        $gambar = gambar::orderBy('created_at','DESC')->take(8)->get();
        return view('guest.index')
        ->with('gambar',$gambar)
        ->with('posting',$posting)
        ;
    }

    public function tentang()
    {
       $item = posting::where('category','tentang')->first();
       return view('about.landing.index')
       ->with('item',$item);
    }

    public function berita()
    {
        $data = posting::orderBy('created_at','DESC')->where('category','!=','tentang')->paginate(12);
        return view('guest.berita.index')
        ->with('data',$data);
    }

    public function singleberita($id)
    {
        $item = posting::find($id);
        return view('guest.berita.singleberita')
        ->with('item',$item);
    }

    public function gallery()
    {
       $data = gambar::orderBy('created_at','DESC')->paginate('15');
       return view('guest.gallery.index')
       ->with('data',$data);

    }
}
