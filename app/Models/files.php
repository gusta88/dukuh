<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class files extends Model
{
    use HasFactory;

    protected $table = "files";
    protected $primaryKey = "file_id";


    public function posting()
    {
        return $this->belongsTo('App\Models\posting', 'id_posting', 'posting_id');
    }

}
