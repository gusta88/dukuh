<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class gambar extends Model
{
    use HasFactory;
    protected $table = 'gambars';
    protected $primaryKey = "gambar_id";

    public function posting()
    {
        return $this->belongsTo('App\Models\posting', 'id_posting', 'posting_id');
    }


}
