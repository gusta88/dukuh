const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        require('postcss-import'),
        require('tailwindcss'),
    ])
    .webpackConfig(require('./webpack.config'));

mix.styles(['public/assets-guest/css/aos.css',
            'public/assets-guest/css/bootstrap.min.css',
            'public/assets-guest/css/imp.css',
            'public/assets-guest/css/custom-animate.css',
            
            'public/assets-guest/css/owl.css',
            'public/assets-guest/css/magnific-popup.css',
            'public/assets-guest/css/scrollbar.css',
            'public/assets-guest/css/hiddenbar.css',
            'public/assets-guest/css/color.css',
            'public/assets-guest/css/style.css',
            'public/assets-guest/css/responsive.css'
        ],'public/asset-guest/sass/app.css');



