<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\PostingController;
use App\Http\Controllers\GambarController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [GuestController::class,'index']);
Route::get('tentang',[GuestController::class,'tentang']);
Route::get('/berita',[GuestController::class,'berita'])->name('berita');
Route::get('/singleberita/{id}',[GuestController::class,'singleberita'])->name('singleberita');
Route::get('/gallery',[GuestController::class,'gallery']);
Route::middleware(['auth:sanctum', 'verified'])->group(function(){

    Route::get('/dashboard', [DashboardController::class,'index'])->name('dashboard');
    Route::resource('/posting', PostingController::class);
    Route::resource('/gambar',GambarController::class);
    Route::get('/admin/tentang',[PostingController::class,'tentang'])->name('admin.tentang');
    Route::POST('/admin/tentang',[PostingController::class,'addtentang'])->name('addtentang');
});
